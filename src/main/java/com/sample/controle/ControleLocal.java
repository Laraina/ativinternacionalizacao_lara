package com.sample.controle;

import java.io.Serializable;
import java.util.Locale;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

@Named(value = "controleLocal")
@SessionScoped
public class ControleLocal implements Serializable{
    
 private Locale currentLocale = new Locale("pt", "BR");

 public void englishLocale() {
  UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
  currentLocale = Locale.US;
  viewRoot.setLocale(currentLocale);
 }

 public void portugueseLocale() {
  UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
  currentLocale = new Locale("pt", "BR");
  viewRoot.setLocale(currentLocale);
 }

 public Locale getCurrentLocale() {
  return currentLocale;
 }
}

